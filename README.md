## Commerce Mpesa
This module helps drupal commerce merchants accept M-Pesa payments.

##### Installation
- [Download from Drupal.org](https://www.drupal.org/project/2834227/git-instructions "Drupal.org")

Install it as you would any other Drupal module. 

##### Dependencies
- Drupal Commerce

##### Configuration
The configuration options for this module follows the standard procedure for the Drupal Commerce payment modules.

The following options are available;
- **Kopokopo Secret**: The api secret from Kopokopo. This is available from the API settings on kopokopo.
- **Paybill Number**: The paybill/till number that is receiving the payments
- **Mpesa Provider**: At the moment only till numbers obtained from Kopokopo are supported

##### Kopokopo Configuration
Some config changes will need to be made on the Kopokopo API.Kopokopo needs to be aware of the URL that it will send data to.
This module relies on the PUSH api provided by Kopokopo.

On your kopokopo account you need to get to the 'Api settings' page.

We only need to provide the callback url and leave the rest as is. Your callback url should be in the pattern of '*your-domain.com*__/mpesa/callback__'

 

That's it,you are ready to receive payments.

__TODO__
- Release on drupal.org
- Better documentation
- Support transactions from MPESA directly